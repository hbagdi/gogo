# gogo

Coming soon!

[![GoDoc](https://godoc.org/github.com/hbagdi/gogo?status.svg)](https://godoc.org/github.com/hbagdi/gogo)
[![Build Status](https://travis-ci.org/hbagdi/gogo.svg?branch=master)](https://travis-ci.org/hbagdi/gogo) 
[![Coverage Status](https://coveralls.io/repos/github/hbagdi/gogo/badge.svg?branch=master)](https://coveralls.io/github/hbagdi/gogo?branch=master)
[![codecov](https://codecov.io/gh/hbagdi/gogo/branch/master/graph/badge.svg)](https://codecov.io/gh/hbagdi/gogo)
[![Go Report Card](https://goreportcard.com/badge/github.com/hbagdi/gogo)](https://goreportcard.com/report/github.com/hbagdi/gogo)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6699b44af09e4a73a64b73f53d7d5642)](https://www.codacy.com/app/hardikbagdi/gogo?utm_source=github.com&utm_medium=referral&utm_content=hbagdi/gogo&utm_campaign=badger)
[![codebeat badge](https://codebeat.co/badges/84f6a19e-112a-4a16-867f-52e429846ef5)](https://codebeat.co/projects/github-com-hbagdi-gogo-master)
