package files

import (
	"os"
	"path/filepath"
)

// Search is dummy
func Search() string {
	return "gogo"
}

// List lists all files in a directory
func List() (<-chan *os.FileInfo, error) {
	files := make(chan *os.FileInfo)
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	go func() {
		err = filepath.Walk(cwd, func(path string, info os.FileInfo, err error) error {
			files <- &info
			return nil
		})
		close(files)
	}()
	return files, nil
}
