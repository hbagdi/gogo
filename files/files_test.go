package files

import (
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	err := os.Chdir("../testdata")
	if err != nil {
		log.Fatalln(err)
	}
	os.Exit(m.Run())
}

func TestSearch(T *testing.T) {
	assert := assert.New(T)
	s := Search()
	assert.Equal(s, "gogo")
}

func TestList(T *testing.T) {
	files, err := List()
	assert := assert.New(T)
	assert.Nil(err)
	assert.NotNil(files)
	file := <-files
	assert.NotNil(file)
	log.Println((*file).Name())
	file = <-files
	assert.NotNil(file)
	log.Println((*file).Name())
	for file = range files {
	}
}
