package main

import (
	"fmt"

	"github.com/hbagdi/gogo/files"
)

func main() {
	files.Search()
	files, err := files.List()
	if err != nil {
		panic(err)
	}
	for file := range files {
		fmt.Println((*file).Name(), (*file).IsDir())
	}

}
