#!/usr/bin/env bash

#code-climate
curl -L https://codeclimate.com/downloads/test-reporter/test-reporter-latest-linux-amd64 > ./cc-test-reporter
chmod +x ./cc-test-reporter
./cc-test-reporter before-build

set -e

for d in $(go list ./... | grep -v vendor); do
    go test -v -coverprofile="profile.out" -covermode=atomic $d
    if [ -f profile.out ]; then
        cat profile.out >> coverage.txt
        rm profile.out
    fi
done
/bin/cp coverage.txt c.out
./cc-test-reporter after-build
